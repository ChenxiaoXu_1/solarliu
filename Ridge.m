clc
clear all
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%synthetics data
load 'data_np.mat';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

X = X_np;
Y = Y_np;

[m, p, n] = size(X_np);

trainPart = 0.8;
X_np = reshape(X_np, m, n*p);

% %matlabpool open;
% 
% %T is time length
% %p is model order
% %n is gene number
% %m=T-p

Xtrain = X(1:floor(trainPart*m),:);
Ytrain = Y(1:floor(trainPart*m),:);

Xvalid = X(floor(trainPart*m)+1:end,:);
Yvalid = Y(floor(trainPart*m)+1:end,:);

%grid search to pich the best lambda
lambda_set = [0.001, 0.01, 0.1, 1, 10, 100]';
error_train_set = zeros(size(lambda_set));
error_valid_set = zeros(size(lambda_set));

for i = 1:size(lambda_set,1)
    B=(Xtrain'*Xtrain+lambda_set(i)*eye(size(Xtrain,2)))\Xtrain'*Ytrain;
    
    Yhat_train = Xtrain*B;  
    error_train_set(i) = sum(sum(Yhat_train - Ytrain).^2)/(m*n);
  
    Yhat_valid = Xvalid*B;  
    error_valid_set(i) = sum(sum(Yhat_valid - Yvalid).^2)/(m*n);    
end

lambda = lambda_set(find(error_valid_set == min(error_valid_set)));
B = (Xtrain'*Xtrain+lambda*eye(size(Xtrain,2)))\Xtrain'*Ytrain;

%causal analysis
B=abs(B);

EstimateGraph=zeros(n);
for i=1:p
    EstimateGraph=max(EstimateGraph, B(((i-1)*n+1):((i-1)*n+n) ,:));
end
figure(1)
imagesc(EstimateGraph)
%save EstimateGraphVar EstimateGraph;
% 
% figure(2)
% groundtruth = A;
% imagesc(A)
% 
% groundTruthVector = reshape(groundtruth, n*n, 1);
% estimateVector = reshape(EstimateGraph, n*n, 1);
% 
% save('groundTruthVector', 'groundTruthVector')
% save('estimateVector', 'estimateVector')

% 
% [X, Y, T, AUROC] = perfcurve(groundTruthVector, estimateVector, 1);
% [X, Y, T, AUPR] = perfcurve(groundTruthVector, estimateVector, 1, 'XCrit', 'tpr', 'YCrit', 'prec');


